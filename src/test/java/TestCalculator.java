import org.junit.Assert;
import org.junit.Test;

public class TestCalculator {

  @Test
  public void test01() {
    int result = Calculator.add(1, 2);
    Assert.assertEquals(3, result);
  }
}
